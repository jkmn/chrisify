package main

import (
	"flag"
	"github.com/disintegration/imaging"
	"github.com/jackmanlabs/errors"
	"github.com/zikes/chrisify/facefinder"
	"image"
	"image/draw"
	"image/jpeg"
	_ "image/png"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

//go:generate ./haar_cascade.sh

var (
	facesPath      = flag.String("faces", "", "The directory to search for faces.")
	inputFilename  = flag.String("i", "", "The source file containing the image onto which you want to paste faces.")
	outputFilename = flag.String("o", "", "The file to which you want your new image written, '-' for stderr.")
)

func main() {
	flag.Parse()

	var (
		faceList FaceList
		err      error
	)

	if len(*facesPath) == 0 {
		flag.Usage()
		log.Fatal("The 'faces' argument is required.")
	}

	*facesPath, err = filepath.Abs(*facesPath)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	err = faceList.Load(*facesPath)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if len(faceList) == 0 {
		flag.Usage()
		log.Fatal("The 'faces' directory provided did not contain any faces in PNG format.")
	}

	// This is some trickery to make the Haar Cascade XML hidden to the user.

	haarFile, err := ioutil.TempFile("./", "haarfile")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
	haarFileName := haarFile.Name()
	defer os.Remove(haarFileName)

	_, err = haarFile.WriteString(haarCascadeXml)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	err = haarFile.Close()
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	finder := facefinder.NewFinder(haarFileName)

	// Load the base image and detect faces.

	baseImage, err := loadImage(*inputFilename)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	targetFaces := finder.Detect(baseImage)

	bounds := baseImage.Bounds()

	canvas := canvasFromImage(baseImage)

	for _, face := range targetFaces {
		rect := rectMargin(30.0, face)

		newFace := faceList.Random()
		if newFace == nil {
			log.Fatal(errors.New("Nil face returned!"))
		}
		chrisFace := imaging.Fit(newFace, rect.Dx(), rect.Dy(), imaging.Lanczos)

		draw.Draw(
			canvas,
			rect,
			chrisFace,
			bounds.Min,
			draw.Over,
		)
	}

	if len(targetFaces) == 0 {
		face := imaging.Resize(
			faceList[0],
			bounds.Dx()/3,
			0,
			imaging.Lanczos,
		)
		face_bounds := face.Bounds()
		draw.Draw(
			canvas,
			bounds,
			face,
			bounds.Min.Add(image.Pt(-bounds.Max.X/2+face_bounds.Max.X/2, -bounds.Max.Y+int(float64(face_bounds.Max.Y)/1.9))),
			draw.Over,
		)
	}

	var outputFile io.WriteCloser
	if *outputFilename == "-" {
		outputFile = os.Stderr
	} else {
		outputFile, err = os.Create(*outputFilename)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}

		defer outputFile.Close()
	}

	jpeg.Encode(outputFile, canvas, &jpeg.Options{jpeg.DefaultQuality})
}
