package main

import (
	"image"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/disintegration/imaging"
	"github.com/jackmanlabs/errors"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type Face struct {
	image.Image
}

func (f *Face) LoadFile(file string) error {
	reader, err := os.Open(file)
	if err != nil {
		return err
	}
	f.Image, _, err = image.Decode(reader)
	if err != nil {
		return err
	}
	return nil
}

func NewFace(file string) (*Face, error) {
	face := &Face{}
	if err := face.LoadFile(file); err != nil {
		return face, err
	}
	return face, nil
}

func NewMustFace(file string) *Face {
	face, err := NewFace(file)
	if err != nil {
		panic(err)
	}
	return face
}

type FaceList []*Face

func (faceList FaceList) Random() image.Image {
	i := rand.Intn(len(faceList))
	face := faceList[i]
	if rand.Intn(2) == 0 {
		return imaging.FlipH(face.Image)
	}
	return face.Image
}

func (faceList *FaceList) Load(dir string) error {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return errors.Stack(err)
	}
	for _, file := range files {
		if filepath.Ext(file.Name()) == ".png" {
			face, err := NewFace(path.Join(dir, file.Name()))
			if err != nil {
				return errors.Stack(err)
			}
			*faceList = append(*faceList, face)
		}
	}
	return nil
}
