package main

import (
	"github.com/jackmanlabs/errors"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os/exec"
	"path/filepath"
	"time"
)

var filenames = []string{
	"have-a-good-day-have-it.jpg",
	"pro-smite-player.jpg",
	"esports-legend.jpg",
	"awkward-dancer.jpg",
	"Malakhor.jpg",
	"seeya-very-much.jpg",
	"presumably.jpg",
	"that-sounded-weird-but-im-sticking-to-it.jpeg",
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func randomFile() string {
	return RandStringRunes(6) + "-" + filenames[rand.Intn(len(filenames))]
}

func SaveFile(b []byte) string {
	file := randomFile()
	path := filepath.Join(*destPath, file)
	ioutil.WriteFile(path, b, 0644)
	return *webPath + "/" + file
}

func GetFile(file File) (io.Reader, error) {
	client := &http.Client{
		Timeout: time.Second * 20,
	}
	request, err := http.NewRequest(http.MethodGet, file.URLPrivateDownload, nil)
	if err != nil {
		return nil, errors.Stack(err)
	}
	request.Header.Add("Authorization", "Bearer "+*token)
	response, err := client.Do(request)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return response.Body, nil
}

func Chrisify(inputPath, outputPath string) error {
	err := exec.Command(*chrisifyPath, "-i", inputPath, "-o", outputPath, "-faces", *facesPath).Run()
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}
