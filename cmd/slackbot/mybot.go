// https://www.opsdash.com/blog/slack-bot-in-golang.html

package main

import (
	"encoding/json"
	"flag"
	"github.com/jackmanlabs/errors"
	"github.com/pborman/uuid"
	"io"
	"log"
	"os"
	"strings"
)

var (
	token        *string = flag.String("token", "", "The Slack API token for the bot.")
	chrisifyPath *string = flag.String("chrisify", "", "The path to the chrisify binary.")
	facesPath    *string = flag.String("faces", "", "The path to a directory containing source faces.")
	destPath     *string = flag.String("output", "", "The local path of a directory where the resulting images should be saved.")
	webPath      *string = flag.String("url", "", "The HTTP base path that points to the local output directory.")
)

func main() {
	flag.Parse()

	if len(*token) == 0 || len(*chrisifyPath) == 0 || len(*facesPath) == 0 || len(*destPath) == 0 || len(*webPath) == 0 {
		flag.Usage()
		log.Fatal("All arguments are required.")
	}

	*webPath = strings.TrimSuffix(*webPath, "/")

	// start a websocket-based Real Time API session
	ws, id, err := slackConnect(*token)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
	log.Println("slackbot ready, ^C exits")
	log.Print("Slackbot ID: ", id)

	for {
		// read each incoming message
		m, err := getMessage(ws)
		if err != nil {
			log.Fatal(err)
		}

		log.Print("Got a message!")
		log.Print(m.Text)

		// see if we're mentioned
		if m.Type == "message" && m.SubType == "file_share" && strings.Contains(m.Text, "<@"+id+">") {
			var channel string
			json.Unmarshal(m.Channel, &channel)
			uuid_ := uuid.New()
			outputFileName := uuid_ + ".new.jpg"
			outputFilePath := *destPath + "/" + outputFileName
			inputFileName := uuid_ + ".old.jpg"
			inputFilePath := *destPath + "/" + inputFileName
			inputReader, err := GetFile(m.File)
			if err != nil {
				log.Print(errors.Stack(err))
			}

			inputFile, err := os.Create(inputFileName)
			if err != nil {
				log.Print(errors.Stack(err))
			}

			_, err = io.Copy(inputFile, inputReader)
			if err != nil {
				log.Print(errors.Stack(err))
			}

			err = Chrisify(inputFilePath, outputFilePath)
			if err != nil {
				log.Print(errors.Stack(err))
			}

			log.Printf("Uploading to %s", channel)

			postMessage(ws, map[string]string{
				"type":    "message",
				"text":    *webPath + "/" + outputFileName,
				"channel": channel,
			})
		}
	}
}
