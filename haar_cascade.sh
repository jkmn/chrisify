#!/usr/bin/env bash

echo 'package main' > haar_cascade.go
echo 'var haarCascadeXml string = `' >> haar_cascade.go
cat haarcascade_frontalface_alt.xml >> haar_cascade.go
echo '`' >> haar_cascade.go
gofmt -w ./haar_cascade.go